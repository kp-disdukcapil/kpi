<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller

{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_login');
    }

    public function index()
    {

        $this->load->view('templates/L_header');
        $this->load->view('administrator/v_login');
        $this->load->view('templates/V_footer');
    }
    public function auth_login()
    {
        $this->_login();
    }
    private function _login()
    {
        $username = $this->input->post('username', TRUE);
        $password = md5($this->input->post('password', TRUE));
        $akses = $this->input->post('akses', TRUE);
        $validate = $this->M_login->validate($username, $password, $akses);
        if ($validate->num_rows() > 0) {
            $data  = $validate->row_array();
            $user_id  = $data['user_id'];
            $username  = $data['username'];
            $akses = $data['akses'];
            $sesdata = array(
                'user_id'  => $user_id,
                'username'  => $username,
                'akses' => $akses,
                'logged_in' => TRUE
            );
            $this->session->set_userdata($sesdata);
            if ($akses === 'administrator') {
                redirect('dashboard');
            } else if ($akses === 'user') {
                redirect('dashboard');
            }
        } else {
            echo $this->session->set_flashdata('msg', 'Username or Password is Wrong');
            redirect('Login');
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('akses');
        $this->session->unset_userdata('loggin_in');
        redirect('Login');
    }
}
