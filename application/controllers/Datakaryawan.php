<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datakaryawan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_user');
    }
    public function index()
    {
        $data['list_user'] = $this->M_user->get_all();
        $data['list_user'] = $this->M_user->getjoin();
        $data['title'] = 'List User';
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/karyawan_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['list_user'] = $this->M_user->get_all();
        $data['list_divisi'] = $this->M_user->getdivisi();
        $data['list_jabatan'] = $this->M_user->getjabatan();
        $data['username'] = $this->session->userdata();


        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/karyawan_tambah', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_ubah($id = '')
    {
        $data['data_karyawan'] = $this->M_user->get_by_id($id);
        $data['list_divisi'] = $this->M_user->getdivisi();
        $data['list_jabatan'] = $this->M_user->getjabatan();
        $data['username'] = $this->session->userdata();


        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/karyawan_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nik            = $this->input->post('nik');
        $nama_user   = $this->input->post('nama_karyawan');
        $jk             = $this->input->post('jk');
        $tgl_lahir      = $this->input->post('tgl_lahir');
        $divisi_id   = $this->input->post('id_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');
        $no_telp        = $this->input->post('no_telp');
        $email          = $this->input->post('email');
        $tgl_kerja      = $this->input->post('tgl_kerja');
        $username     = $this->input->post('username');
        $password     = $this->input->post('password');
        $akses        = $this->input->post('akses');

        $password = md5($password);
        $simpan = array(
            'nik' => $nik,
            'jk' => $jk,
            'nama_user' => $nama_user,
            'tgl_lahir' => $tgl_lahir,
            'no_telp' => $no_telp,
            'email' => $email,
            'tgl_kerja' => $tgl_kerja,
            'username' => $username,
            'password' => $password,
            'akses' => $akses,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,

        );
        $this->M_user->insert($simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Ditambahkan!
        </div>');

        redirect('Datakaryawan');
    }

    public function proses_ubah_data()
    {
        $karyawan_id            = $this->input->post('karyawan_id');
        $nik            = $this->input->post('nik');
        $nama_user      = $this->input->post('nama_karyawan');
        $jk             = $this->input->post('jk');
        $tgl_lahir      = $this->input->post('tgl_lahir');
        $divisi_id      = $this->input->post('divisi_id');
        $jabatan_id   = $this->input->post('jabatan_id');
        $no_telp        = $this->input->post('no_telp');
        $email          = $this->input->post('email');
        $tgl_kerja      = $this->input->post('tgl_kerja');
        $username     = $this->input->post('username');
        $password     = $this->input->post('password');
        $akses        = $this->input->post('akses');

        $simpan = array(
                'nik' => $nik,
                'jk' => $jk,
                'nama_user' => $nama_user,
                'tgl_lahir' => $tgl_lahir,
                'no_telp' => $no_telp,
                'email' => $email,
                'tgl_kerja' => $tgl_kerja,
                'username' => $username,
                'akses' => $akses,
                'divisi_id' => $divisi_id,
                'jabatan_id' => $jabatan_id,
        );

		if($password != '') {
			$password = md5($password);
			$simpan['password'] = $password;
		}

        $this->M_user->edit($karyawan_id, $simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Diubah!
        </div>');

        redirect('Datakaryawan');
    }

    public function proses_hapus_data($user_id)
    {
        $this->M_user->delete($user_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Dihapus!
        </div>');

        redirect('Datakaryawan');
    }
}
