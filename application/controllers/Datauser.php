<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datauser extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_user');
    }

    public function index()
    {
        $data['list_user'] = $this->M_user->get_all();
        $data['title'] = 'List User';
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/user_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/user_tambah');
        $this->load->view('templates/V_footer');
    }

    public function form_ubah($id = '')
    {
        $data['data_user'] = $this->M_user->get_by_id($id);
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/user_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nama_user    = $this->input->post('nama_user');
        $username     = $this->input->post('username');
        $password     = $this->input->post('password');
        $akses        = $this->input->post('akses');

        $newpass = md5($password);
        $simpan = array(
            'nama_user' => $nama_user,
            'username' => $username,
            'password' => $newpass,
            'akses' => $akses,
        );
        $this->M_user->insert($simpan);

        redirect('Datauser');
    }

    public function proses_ubah_data()
    {
        $user_id      = $this->input->post('user_id');
        $nama_user    = $this->input->post('nama_user');
        $username     = $this->input->post('username');
        $password     = $this->input->post('password');

        $simpan = array(
            'nama_user' => $nama_user,
            'username' => $username,
            'password' => $password,
        );
        $this->M_user->edit($user_id, $simpan);

        redirect('Datauser');
    }

    public function proses_hapus_data($user_id)
    {
        $this->M_user->delete($user_id);

        redirect('Datauser');
    }
}
