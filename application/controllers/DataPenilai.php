<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DataPenilai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_penilai');
    }
    public function index()
    {
        $data['list_penilai'] = $this->M_penilai->getjoin();
        $data['title'] = 'List penilai';
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/penilai_data');
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['list_penilai'] = $this->M_penilai->getjoin();
        $data['list_divisi'] = $this->M_penilai->getdivisi();
        $data['list_jabatan'] = $this->M_penilai->getjabatan();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/penilai_tambah', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_ubah($id = '')
    {
        $data['data_penilai'] = $this->M_penilai->get_by_id($id);
        $data['list_divisi'] = $this->M_penilai->getdivisi();
        $data['list_jabatan'] = $this->M_penilai->getjabatan();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/penilai_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nama   = $this->input->post('nama');
        $divisi_id   = $this->input->post('id_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');

        $simpan = array(

            'nama' => $nama,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,

        );
        $this->M_penilai->insert($simpan);

        redirect('DataPenilai');
    }

    public function proses_ubah_data()
    {
        $penilai_id    = $this->input->post('penilai_id');
        $nama          = $this->input->post('nama');
        $divisi_id     = $this->input->post('id_divisi');
        $jabatan_id    = $this->input->post('id_jabatan');

        $simpan = array(
            'penilai_id' => $penilai_id,
            'nama' => $nama,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,

        );

        // print_r($simpan);
        // die;

        $this->M_penilai->edit($penilai_id, $simpan);

        redirect('DataPenilai');
    }

    public function proses_hapus_data($penilai_id)
    {
        // print_r($penilai_id);
        // die;
        $this->M_penilai->delete($penilai_id);

        redirect('DataPenilai');
    }
}
