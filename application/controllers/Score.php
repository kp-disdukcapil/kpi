<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Score extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_score');
    }
    public function index()
    {

        $data['list_score'] = $this->M_score->getjoin();
        $data['title'] = 'List score';
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/score_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['list_score'] = $this->M_score->getjoin();
        $data['list_karyawan'] = $this->M_score->getkaryawan();
        $data['list_penilai'] = $this->M_score->getpenilai();
        $data['username'] = $this->session->userdata();


        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/score_tambah', $data);
        $this->load->view('templates/V_footer');
    }

    public function fillscore()
    {
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/score_fill');
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $tgl_penilaian    = $this->input->post('tgl_penilaian');
        $karyawan_id   = $this->input->post('karyawan_id');
        $penilai_id   = $this->input->post('penilai_id');


        $simpan = array(
            'tgl_penilaian' => $tgl_penilaian,
            'karyawan_id' => $karyawan_id,
            'penilai_id' => $penilai_id,
        );
        $this->M_score->insert($simpan);

        redirect('score');
    }

    public function proses_ubah_data()
    {
        $grup_id      = $this->input->post('grup_id');
        $nama_grup    = $this->input->post('nama_grup');
        $divisi_id   = $this->input->post('id_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'grup_id' => $grup_id,
            'nama_grup' => $nama_grup,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,
            'deskripsi' => $deskripsi,
        );

        $this->M_score->edit($grup_id, $simpan);

        redirect('KpiGrup');
    }

    public function proses_hapus_data($grup_id)
    {
        $this->M_score->delete($grup_id);

        redirect('KpiGrup');
    }
}
