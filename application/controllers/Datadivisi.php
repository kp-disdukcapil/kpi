<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datadivisi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_divisi');
    }

    public function index()
    {

        $data['list_divisi'] = $this->M_divisi->getjoin();
        $data['title'] = 'List Divisi';
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/divisi_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function export()
    {

        $data['list_divisi'] = $this->M_divisi->get_all();
        $data['title'] = 'List Divisi';

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/divisi-export');
        $this->load->view('templates/V_footer');
    }

    public function form_tambah()
    {
        $data['list_jabatan'] = $this->M_divisi->getjabatan();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/divisi_tambah', $data);
        $this->load->view('templates/V_footer');
    }

    public function form_ubah($id = '')
    {
        $data['data_divisi'] = $this->M_divisi->get_by_id($id);
        $data['list_jabatan'] = $this->M_divisi->getjabatan();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/divisi_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nama_divisi    = $this->input->post('nama_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'nama_divisi' => $nama_divisi,
            'jabatan_id' => $jabatan_id,
            'deskripsi' => $deskripsi,
        );
        $this->M_divisi->insert($simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Ditambahkan!
        </div>');

        redirect('Datadivisi');
    }

    public function proses_ubah_data()
    {
        $divisi_id      = $this->input->post('divisi_id');
        $nama_divisi    = $this->input->post('nama_divisi');
        $jabatan_id   = $this->input->post('jabatan_id');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'nama_divisi' => $nama_divisi,
            'jabatan_id' => $jabatan_id,
            'deskripsi' => $deskripsi,
        );
        $this->M_divisi->edit($divisi_id, $simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Diubah!
        </div>');

        redirect('Datadivisi');
    }

    public function proses_hapus_data($divisi_id)
    {
        $this->M_divisi->delete($divisi_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Dihapus!
        </div>');

        redirect('Datadivisi');
    }
}
