<?php
defined('BASEPATH') or exit('No direct script access allowed');

class FillScore extends CI_Controller
{
    public function index()
    {
        $this->load->view('templates/V_HeadSidebar');
        $this->load->view('templates/V_sidebar');
        $this->load->view('templates/V_header');
        $this->load->view('administrator/FillScore');
        $this->load->view('templates/V_FootSidebar');
        $this->load->view('templates/V_footer');
    }
}
