<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KpiGrup extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_grup');
    }
    public function index()
    {

        $data['list_grup'] = $this->M_grup->getjoin();
        $data['title'] = 'List grup_kpi';
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/kpigrup_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['list_grup'] = $this->M_grup->getjoin();
        $data['list_divisi'] = $this->M_grup->getdivisi();
        $data['list_jabatan'] = $this->M_grup->getjabatan();
        $data['username'] = $this->session->userdata();


        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/kpigrup_tambah', $data);
        $this->load->view('templates/V_footer');
    }

    public function form_ubah($id = '')
    {
        $data['data_grup_kpi'] = $this->M_grup->get_by_id($id);
        $data['list_divisi'] = $this->M_grup->getdivisi();
        $data['list_jabatan'] = $this->M_grup->getjabatan();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/kpigrup_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nama_grup    = $this->input->post('nama_grup');
        $divisi_id   = $this->input->post('id_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'nama_grup' => $nama_grup,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,
            'deskripsi' => $deskripsi,
        );
        $this->M_grup->insert($simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Ditambahkan!
        </div>');

        redirect('KpiGrup');
    }

    public function proses_ubah_data()
    {
        $grup_id      = $this->input->post('grup_id');
        $nama_grup    = $this->input->post('nama_grup');
        $divisi_id   = $this->input->post('id_divisi');
        $jabatan_id   = $this->input->post('id_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'grup_id' => $grup_id,
            'nama_grup' => $nama_grup,
            'divisi_id' => $divisi_id,
            'jabatan_id' => $jabatan_id,
            'deskripsi' => $deskripsi,
        );

        $this->M_grup->edit($grup_id, $simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Diubah!
        </div>');

        redirect('KpiGrup');
    }

    public function proses_hapus_data($grup_id)
    {
        $this->M_grup->delete($grup_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Dihapus!
        </div>');

        redirect('KpiGrup');
    }
}
