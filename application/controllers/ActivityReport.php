<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ActivityReport extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_activity');
    }

    public function index()
    {
        $data['list_activity'] = $this->M_activity->getjoin();
		$data['list_user'] = $this->M_activity->getuser();
        $data['list_divisi'] = $this->M_activity->getdivisi();
		$data['list_grup'] = $this->M_activity->getgrup();
		$data['list_item'] = $this->M_activity->getitem();
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/activity-report', $data);
        $this->load->view('templates/V_footer');
    }
	public function export()
	{
		$periode_start = $this->input->post('periode_start');
		$periode_end = $this->input->post('periode_end');
		$id_user = $this->input->post('id_user');
		$id_divisi = $this->input->post('id_divisi');
		$id_grup = $this->input->post('id_grup');
		$id_item = $this->input->post('id_item');

		$cond = array(
			'periode_start' => $periode_start,
			'periode_end' => $periode_end,
			'id_user' => $id_user,
			'id_divisi' => $id_divisi,
			'id_grup' => $id_grup,
			'id_item' => $id_item,
		);

		$data['list_laporan'] = $this->M_activity->get_condition($cond);
		$this->load->view('administrator/activity-export', $data);
	}
    public function export_all()
    {
        $data['list_laporan'] = $this->M_activity->getjoin();
        $this->load->view('administrator/activity-export', $data);
    }
}
