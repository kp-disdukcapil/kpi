<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Datajabatan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_jabatan');
    }

    public function index()
    {

        $data['list_jabatan'] = $this->M_jabatan->get_all();
        $data['title'] = 'List Jabatan';
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/jabatan_data', $data);
        $this->load->view('templates/V_footer');
    }


    public function form_tambah()
    {
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/jabatan_tambah');
        $this->load->view('templates/V_footer');
    }

    public function form_ubah($id = '')
    {
        $data['username'] = $this->session->userdata();
        $data['data_jabatan'] = $this->M_jabatan->get_by_id($id);

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/jabatan_ubah', $data);
        $this->load->view('templates/V_footer');
    }

    public function proses_tambah_data()
    {
        $nama_jabatan   = $this->input->post('nama_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'nama_jabatan' => $nama_jabatan,
            'deskripsi' => $deskripsi,
        );
        $this->M_jabatan->insert($simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Ditambahkan!
        </div>');

        redirect('Datajabatan');
    }

    public function proses_ubah_data()
    {
        $jabatan_id      = $this->input->post('jabatan_id');
        $nama_jabatan   = $this->input->post('nama_jabatan');
        $deskripsi      = $this->input->post('deskripsi');

        $simpan = array(
            'nama_jabatan' => $nama_jabatan,
            'deskripsi' => $deskripsi,
        );
        $this->M_jabatan->edit($jabatan_id, $simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Diubah!
        </div>');

        redirect('Datajabatan');
    }

    public function proses_hapus_data($jabatan_id)
    {
        $this->M_jabatan->delete($jabatan_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Dihapus!
        </div>');

        redirect('Datajabatan');
    }
}
