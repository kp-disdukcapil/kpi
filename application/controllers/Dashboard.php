<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('M_dashboard');
		$this->load->model('M_item');
		$this->load->model('M_grup');
		check_login();
	}
	public function index()
	{
		if ($this->session->userdata('akses') === 'administrator') {
			$data['username'] = $this->session->userdata();
			$this->load->view('templates/V_header', $data);
			$this->load->view('templates/V_sidebar');
			$this->load->view('administrator/v_dashboard', $data);
			$this->load->view('templates/V_footer');
		} else if ($this->session->userdata('akses') === 'user') {
			$data['list_activity'] = $this->M_dashboard->getjoin();
			$data['username'] = $this->session->userdata();
			$this->load->view('templates/V_header', $data);
			$this->load->view('templates/V_Usidebar');
			$this->load->view('karyawan/activity_data', $data);
			$this->load->view('templates/V_footer');
		} else {
			redirect('Login');
		}
	}
	public function form_tambah()
	{
		$data['list_item'] = $this->M_dashboard->getitem();
		$data['username'] = $this->session->userdata();
		$this->load->view('templates/V_header', $data);
		$this->load->view('templates/V_Usidebar');
		$this->load->view('karyawan/activity_tambah', $data);
		$this->load->view('templates/V_footer');
	}
	public function form_ubah($id = '')
	{
		$data['data_activity'] = $this->M_dashboard->get_by_id($id);
		$data['list_item'] = $this->M_dashboard->getitem();
		$data['username'] = $this->session->userdata();

		$this->load->view('templates/V_Usidebar', $data);
		$this->load->view('templates/V_header');
		$this->load->view('karyawan/activity_ubah', $data);
		$this->load->view('templates/V_footer');
	}
	public function proses_tambah_data()
	{
		$item_id   = $this->input->post('parameter');
		$tgl_activity    = $this->input->post('tgl_activity');
		$progres    = $this->input->post('progres');
		$deskripsi      = $this->input->post('deskripsi');
		$user = $this->session->userdata();
		$simpan = array(
			'item_id' => $item_id,
			'tgl_activity' => $tgl_activity,
			'progres' => $progres,
			'deskripsi' => $deskripsi,
			'user_id' => $user['user_id']
		);

		$bool = $this->check_data($simpan);
		if($bool == TRUE) {
			redirect('Dashboard');
			return;
		}
		else {
			$this->M_dashboard->insert($simpan);
			$test = $this->scoring($simpan);
			$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
			Data Berhasil Ditambahkan!
			</div>');

			redirect('Dashboard');
		}
	}
	public function proses_ubah_data()
	{
		$activity_id      = $this->input->post('activity_id');
		$item_id   = $this->input->post('parameter');
		$tgl_activity    = $this->input->post('tgl_activity');
		$progres    = $this->input->post('progres');
		$deskripsi      = $this->input->post('deskripsi');
		$user = $this->session->userdata();

		$simpan = array(
			'activity_id' => $activity_id,
			'item_id' => $item_id,
			'tgl_activity' => $tgl_activity,
			'progres' => $progres,
			'deskripsi' => $deskripsi,
			'user_id' => $user['user_id']
		);
		$this->M_dashboard->edit($activity_id, $simpan);
		$test = $this->scoring($simpan);
		// return;
		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
		Data Berhasil Diubah!
		</div>');

		redirect('Dashboard');
	}
	public function proses_hapus_data($activity_id)
	{
		$this->M_dashboard->delete($activity_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
		Data Berhasil Dihapus!
		</div>');

		redirect('Dashboard');
	}
	public function check_data($data) {
		$res = $this->M_dashboard->maxprogres_by_user_item($data['user_id'], $data['item_id']);
		$res = json_decode(json_encode($res), true);
		if ((int)$data['progres'] <= (int)$res['maxprogres']) {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">
			Data tidak bisa diinput, progres harus bertambah dari yang sebelumnya!
			</div>');

			return TRUE;
		}
		else {
			return FALSE;
		}
	}

		public function scoring($data){
		$progres = array();
		$value = $this->M_dashboard->maxprogres($data['user_id'], $data['item_id']);
		$array = json_decode(json_encode($value), true);
		echo var_dump($array);
		$no = 0;
		$result = array();
		foreach($array as $row){
			$result0 = (int)$row['grup_id'];
			if((int)$row['progres']>=(int)$row['nilai_min']){
				if((int)$row['progres']<=(int)$row['nilai_max']){
					$result1 = (int)$row['progres']/(int)$row['nilai_max']*100*$row['bobot']/100;
					$result[$no] = array('nilai' => $result1);
				}else if((int)$row['progres']>=(int)$row['nilai_max']){
					$result1 = (int)$row['nilai_max']/(int)$row['nilai_max']*100*$row['bobot']/100;
					$result[$no] = array('nilai' => $result1);
				}
			}
			++$no;
		}
		$this->M_item->edit($data['item_id'],$result[0]);
		$sum = $this->M_item->sumnilai($result0);
		$sum = json_decode(json_encode($sum), true);
		// echo var_dump($sum);
		$this->M_grup->edit($result0, $sum);
		// echo var_dump($result);
		}
}
