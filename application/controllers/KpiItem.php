<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KpiItem extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_item');
    }
    public function index()
    {
        $data['list_item'] = $this->M_item->getjoin();
        $data['title'] = 'List item_kpi';
        $data['username'] = $this->session->userdata();
        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/KpiItem_data', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_tambah()
    {
        $data['list_item'] = $this->M_item->getjoin();
        $data['list_grup'] = $this->M_item->getgrup_kpi();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/kpiitem_tambah', $data);
        $this->load->view('templates/V_footer');
    }
    public function form_ubah($id = '')
    {
        $data['data_item_kpi'] = $this->M_item->get_by_id($id);
        $data['list_grup'] = $this->M_item->getgrup_kpi();
        $data['username'] = $this->session->userdata();

        $this->load->view('templates/V_header', $data);
        $this->load->view('templates/V_sidebar');
        $this->load->view('administrator/kpiitem_ubah', $data);
        $this->load->view('templates/V_footer');
    }
    public function proses_tambah_data()
    {
        $grup_id    = $this->input->post('grup_id');
        $parameter = $this->input->post('parameter');
        $bobot = $this->input->post('bobot');
        $nilai_min = $this->input->post('nilai_min');
        $nilai_max = $this->input->post('nilai_max');

        $simpan = array(
            'grup_id' => $grup_id,
            'parameter' => $parameter,
            'bobot' => $bobot,
            'nilai_min' => $nilai_min,
            'nilai_max' => $nilai_max,
        );
        $this->M_item->insert($simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Ditambahkan!
        </div>');

        redirect('KpiItem');
    }
    public function proses_ubah_data()
    {
        $item_id      = $this->input->post('item_id');
        $grup_id      = $this->input->post('grup_id');
        $parameter = $this->input->post('parameter');
        $bobot = $this->input->post('bobot');
        $nilai_min = $this->input->post('nilai_min');
        $nilai_max = $this->input->post('nilai_max');

        $simpan = array(
            'grup_id' => $grup_id,
            'parameter' => $parameter,
            'bobot' => $bobot,
            'nilai_min' => $nilai_min,
            'nilai_max' => $nilai_max,
        );
        $this->M_item->edit($item_id, $simpan);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Diubah!
        </div>');

        redirect('KpiItem');
    }

    public function proses_hapus_data($item_id)
    {
        $this->M_item->delete($item_id);

		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">
        Data Berhasil Dihapus!
        </div>');

        redirect('KpiItem');
    }
}
