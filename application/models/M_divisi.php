<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_divisi extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('divisi_id', 'desc');
        return $this->db->get('divisi')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('divisi_id', $id);
        return $this->db->get('divisi')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('divisi', $simpan);
    }

    function edit($divisi_id, $simpan)
    {
        $this->db->where('divisi_id', $divisi_id);
        $this->db->update('divisi', $simpan);
    }
    function getjoin()
    {
        $this->db->select('d.*,j.nama_jabatan');
        $this->db->join('jabatan j', 'j.jabatan_id=d.jabatan_id', 'left');
        return $this->db->get_where('divisi d')->result();
    }
    function delete($divisi_id)
    {
        $this->db->where('divisi_id', $divisi_id);
        $this->db->delete('divisi');
    }
    function getjabatan()
    {
        return $this->db->get('jabatan')->result();
    }
}
