<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('activity_id', 'desc');
        return $this->db->get('activity')->result();
    }
    function get_by_id($id)
    {
        $this->db->where('activity_id', $id);
        return $this->db->get('activity')->row();
    }
    function insert($simpan)
    {
        $this->db->insert('activity', $simpan);
    }
    function edit($activity_id, $simpan)
    {
        $this->db->where('activity_id', $activity_id);
        $this->db->update('activity', $simpan);
    }

    function getjoin()
    {
        $user = $this->session->userdata();
        $this->db->select('ac.*,gk.nama_grup,ik.parameter');
        $this->db->join('item_kpi ik', 'ik.item_id=ac.item_id', 'left');
		$this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
        $this->db->where('user_id', $user['user_id']);
        return $this->db->get_where('activity ac')->result();
    }
	function getjoin_all()
	{
		$this->db->select('ac.*,gk.nama_grup,ik.parameter,k.nama_user');
		$this->db->join('item_kpi ik', 'ik.item_id=ac.item_id', 'left');
		$this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
		$this->db->join('karyawan k', 'k.user_id=ac.user_id', 'left');
		return $this->db->get_where('activity ac')->result();
	}
    function getitem()
    {
		$user = $this->session->userdata();
		$this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
		$this->db->join('divisi d', 'd.divisi_id=gk.divisi_id', 'left');
		$this->db->join('karyawan k', 'k.divisi_id=d.divisi_id', 'left');
		$this->db->where('k.user_id', $user['user_id']);
        return $this->db->get('item_kpi ik')->result();
    }

    function delete($activity_id)
    {
        $this->db->where('activity_id', $activity_id);
        $this->db->delete('activity');
    }
	function maxprogres_by_user_item($user_id, $item_id)
	{
		$this->db->select('max(progres) as maxprogres');
		$this->db->where('user_id', $user_id);
		$this->db->where('item_id', $item_id);
		return $this->db->get('activity')->row();
	}
    function getactivity(){
        $this->db->select('activity_id', $activity_id);
        $this->db->get('activity');
    }
    function maxprogres($user_id, $item_id){
        $this->db->select('ik.item_id,gk.grup_id,ik.bobot,ik.nilai_max,ik.nilai_min,max(ac.progres) as progres');
        $this->db->join('item_kpi ik', 'ik.item_id=ac.item_id', 'left');
        $this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
        $this->db->join('karyawan k', 'k.user_id=ac.user_id', 'left');
        $this->db->where('ac.item_id', $item_id);
        $this->db->where('ac.user_id', $user_id);
        $this->db->group_by('ac.item_id');
        return $this->db->get_where('activity ac')->result();
    }

}
