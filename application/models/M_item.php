<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_item extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('item_id', 'desc');
        return $this->db->get('item_kpi')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('item_id', $id);
        return $this->db->get('item_kpi')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('item_kpi', $simpan);
    }

    function edit($item_id, $simpan)
    {
        $this->db->where('item_id', $item_id);
        $this->db->update('item_kpi', $simpan);
    }
    function getjoin()
    {
        $this->db->select('ik.*,gk.nama_grup');
        $this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
        return $this->db->get_where('item_kpi ik')->result();
    }
    function getgrup_kpi()
    {
        return $this->db->get('grup_kpi')->result();
    }
    function delete($item_id)
    {
        $this->db->where('item_id', $item_id);
        $this->db->delete('item_kpi');
    }
    function sumnilai($grup_id){
        $this->db->select('sum(nilai) as nilai');
        $this->db->where('grup_id', $grup_id);
        return $this->db->get_where('item_kpi')->row();    
    }
}
