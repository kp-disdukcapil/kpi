<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_grup extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('grup_id', 'desc');
        return $this->db->get('grup_kpi')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('grup_id', $id);
        return $this->db->get('grup_kpi')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('grup_kpi', $simpan);
    }

    function edit($grup_id, $simpan)
    {
        $this->db->where('grup_id', $grup_id);
        $this->db->update('grup_kpi', $simpan);
    }

    function getjoin()
    {
        $this->db->select('gk.*,d.nama_divisi,j.nama_jabatan');
        $this->db->join('divisi d', 'd.divisi_id=gk.divisi_id', 'left');
        $this->db->join('jabatan j', 'j.jabatan_id=gk.jabatan_id', 'left');
        return $this->db->get_where('grup_kpi gk')->result();
    }

    function getdivisi()
    {
        return $this->db->get('divisi')->result();
    }

    function getjabatan()
    {
        return $this->db->get('jabatan')->result();
    }



    function delete($grup_id)
    {
        $this->db->where('grup_id', $grup_id);
        $this->db->delete('grup_kpi');
    }
}
