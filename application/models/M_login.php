<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_login extends CI_Model
{
    function validate($username, $password, $akses)
    {
        $this->db->where('username', $username);
        $this->db->where('password', $password);
        $this->db->where('akses', $akses);
        $result = $this->db->get('karyawan');
        return $result;
    }
}
