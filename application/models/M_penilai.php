<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_penilai extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('penilai_id', 'desc');
        return $this->db->get('penilai')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('penilai_id', $id);
        return $this->db->get('penilai')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('penilai', $simpan);
    }

    function edit($penilai_id, $simpan)
    {
        $this->db->where('penilai_id', $penilai_id);
        $this->db->update('penilai', $simpan);
    }

    function delete($penilai_id)
    {
        $this->db->where('penilai_id', $penilai_id);
        $this->db->delete('penilai');
    }
    function getjoin()
    {
        $this->db->select('p.*,d.nama_divisi,j.nama_jabatan');
        $this->db->join('divisi d', 'd.divisi_id=p.divisi_id', 'left');
        $this->db->join('jabatan j', 'j.jabatan_id=p.jabatan_id', 'left');
        return $this->db->get_where('penilai p')->result();
    }

    function getdivisi()
    {
        return $this->db->get('divisi')->result();
    }

    function getjabatan()
    {
        return $this->db->get('jabatan')->result();
    }
}
