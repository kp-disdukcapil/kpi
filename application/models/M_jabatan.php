<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jabatan extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('jabatan_id', 'desc');
        return $this->db->get('jabatan')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('jabatan_id', $id);
        return $this->db->get('jabatan')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('jabatan', $simpan);
    }

    function edit($jabatan_id, $simpan)
    {
        $this->db->where('jabatan_id', $jabatan_id);
        $this->db->update('jabatan', $simpan);
    }

    function delete($jabatan_id)
    {
        $this->db->where('jabatan_id', $jabatan_id);
        $this->db->delete('jabatan');
    }
}
