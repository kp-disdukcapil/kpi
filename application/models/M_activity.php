<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_activity extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('activity_id', 'desc');
        return $this->db->get('activity')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('activity_id', $id);
        return $this->db->get('activity')->row();
    }
    function getjoin()
    {
        $this->db->select('ac.*,ik.parameter,gk.nama_grup,d.nama_divisi,k.nama_user');
        $this->db->join('item_kpi ik', 'ik.item_id=ac.item_id', 'left');
        $this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
        $this->db->join('divisi d', 'd.divisi_id=gk.divisi_id', 'left');
        $this->db->join('karyawan k', 'k.user_id=ac.user_id', 'left');
        return $this->db->get_where('activity ac')->result();
    }
	function get_condition($cond)
	{
		if (!$cond['periode_start'] == '') {
			$this->db->where('ac.tgl_activity >=', $cond['periode_start']);
		}
		if (!$cond['periode_end'] == '') {
			$this->db->where('ac.tgl_activity <=', $cond['periode_end']);
		}
		if (!is_null($cond['id_divisi'])) {
			$this->db->where('d.divisi_id', $cond['id_divisi']);
		}
		if (!is_null($cond['id_user'])) {
			$this->db->where('k.user_id', $cond['user_id']);
		}
		if (!is_null($cond['id_grup'])) {
			$this->db->where('gk.grup_id', $cond['id_grup']);
		}
		if (!is_null($cond['id_item'])) {
			$this->db->where('ik.item_id', $cond['id_item']);
		}

		$this->db->select('k.nama_user,ac.tgl_activity,d.nama_divisi,gk.nama_grup,ik.parameter,ac.progres,ac.deskripsi');
		$this->db->join('item_kpi ik', 'ik.item_id=ac.item_id', 'left');
		$this->db->join('grup_kpi gk', 'gk.grup_id=ik.grup_id', 'left');
		$this->db->join('divisi d', 'd.divisi_id=gk.divisi_id', 'left');
		$this->db->join('karyawan k', 'k.user_id=ac.user_id', 'left');

		return $this->db->get_where('activity ac')->result();
	}
	function getitem()
	{
		return $this->db->get('item_kpi')->result();
	}
	function getgrup()
	{
		return $this->db->get('grup_kpi')->result();
	}
    function getdivisi()
    {
        return $this->db->get('divisi')->result();
    }
	function getuser()
	{
		$this->db->where('akses','user');
		return $this->db->get('karyawan')->result();
	}
	function delete($id)
	{
		$this->db->where('activity_id', $id);
		$this->db->delete('activity');
	}
	function edit($id, $simpan)
	{
		$this->db->where('activity_id', $id);
		$this->db->update('activity', $simpan);
	}
	function getid(){
		$this->db->select('user_id');
		return $this->db->get('karyawan')->result();
	}
}
