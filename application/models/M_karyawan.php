<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_karyawan extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('karyawan_id', 'desc');
        return $this->db->get('karyawan')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('karyawan_id', $id);
        return $this->db->get('karyawan')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('karyawan', $simpan);
    }

    function edit($karyawan_id, $simpan)
    {
        $this->db->where('karyawan_id', $karyawan_id);
        $this->db->update('karyawan', $simpan);
    }

    function delete($karyawan_id)
    {
        $this->db->where('karyawan_id', $karyawan_id);
        $this->db->delete('karyawan');
    }
    function getjoin()
    {
        $this->db->select('kr.*,d.nama_divisi,j.nama_jabatan');
        $this->db->join('divisi d', 'd.divisi_id=kr.divisi_id', 'left');
        $this->db->join('jabatan j', 'j.jabatan_id=kr.jabatan_id', 'left');
        return $this->db->get_where('karyawan kr')->result();
    }

    function getdivisi()
    {
        return $this->db->get('divisi')->result();
    }

    function getjabatan()
    {
        return $this->db->get('jabatan')->result();
    }
}
