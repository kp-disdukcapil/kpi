<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_score extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('score_id', 'desc');
        return $this->db->get('score')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('score_id', $id);
        return $this->db->get('score')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('score', $simpan);
    }
    function fillscore($simpan)
    {
        $this->db->insert('score', $simpan);
    }
    function delete($score_id)
    {
        $this->db->where('score_id', $score_id);
        $this->db->delete('score');
    }
    function getjoin()
    {
        $this->db->select('sc.*,k.nama_karyawan,p.nama');
        $this->db->join('karyawan k', 'k.karyawan_id=sc.karyawan_id', 'left');
        $this->db->join('penilai p', 'p.penilai_id=sc.penilai_id', 'left');
        return $this->db->get_where('score sc')->result();
    }
    function getkaryawan()
    {
        return $this->db->get('karyawan')->result();
    }
    function getpenilai()
    {
        return $this->db->get('penilai')->result();
    }
}
