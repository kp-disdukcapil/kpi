<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{
    function get_all()
    {
        $this->db->order_by('user_id', 'desc');
        return $this->db->get('karyawan')->result();
    }

    function getjoin()
    {
        $this->db->select('gk.*,d.nama_divisi,j.nama_jabatan');
        $this->db->join('divisi d', 'd.divisi_id=gk.divisi_id', 'left');
        $this->db->join('jabatan j', 'j.jabatan_id=gk.jabatan_id', 'left');
        return $this->db->get_where('karyawan gk')->result();
    }

    function get_by_id($id)
    {
        $this->db->where('user_id', $id);
        return $this->db->get('karyawan')->row();
    }

    function insert($simpan)
    {
        $this->db->insert('karyawan', $simpan);
    }

    function edit($user_id, $simpan)
    {
        $this->db->where('user_id', $user_id);
        $this->db->update('karyawan', $simpan);
    }

    function delete($user_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->delete('karyawan');
    }
    function getdivisi()
    {
        return $this->db->get('divisi')->result();
    }

    function getjabatan()
    {
        return $this->db->get('jabatan')->result();
    }
}
