<?php
defined('BASEPATH') or exit('No direct script access allowed');

class database extends CI_Model
{
    public function rowCount()
    {
        return $this->stmt->rowCount();
    }
}
