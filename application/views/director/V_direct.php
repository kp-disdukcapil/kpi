<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

            <!-- Sidebar Toggle (Topbar) -->
            <form class="form-inline">
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>
            </form>

            <!-- Topbar Search -->
            <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                        </button>
                    </div>
                </div>
            </form>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                    <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-search fa-fw"></i>
                    </a>
                    <!-- Dropdown - Messages -->
                    <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                        <form class="form-inline mr-auto w-100 navbar-search">
                            <div class="input-group">
                                <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fas fa-search fa-sm"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </li>


                <div class="topbar-divider d-none d-sm-block"></div>

                <!-- Nav Item - User Information -->
                <li class="nav-item dropdown no-arrow">
                    <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="false" aria-expanded="true">
                        <span class="mr-2 d-none d-lg-inline text-gray-600 small">Direktur 1</span>
                        <img class="img-profile rounded-circle" src="assets/img/undraw_profile.svg">
                    </a>
                    <!-- Dropdown - User Information -->
                    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="#">
                            <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                            Activity Log
                        </a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="<?= base_url('Login/logout') ?>">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                            Logout
                        </a>
                    </div>
                </li>

            </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-2 text-gray-800">SOP SORAYA BETSHEET</h1>

            <!-- DataTales Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">DataTables SOP</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis SOP</th>
                                    <th>Tanggal Request</th>
                                    <th>By Request</th>
                                    <th>File</th>
                                    <th>Status</th>
                                    <th>aksi</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>No</th>
                                    <th>Jenis SOP</th>
                                    <th>Tanggal Request</th>
                                    <th>Request By</th>
                                    <th>File</th>
                                    <th>Status</th>
                                    <th>aksi</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>1. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>3. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>8. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>9. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td>10. </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td> </td>
                                    <td>
                                        <center>
                                            <button type="button" class="btn btn-outline-success" data-toggle="modal" data-target="#YesModal">Yes</button>
                                            <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#NoModal">No</button>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

        <!-- Footer -->
        <footer class="sticky-footer bg-white">
            <div class="container my-auto">
                <div class="copyright text-center my-auto">
                    <span>Soraya Betsheet &copy; 2022</span>
                </div>
            </div>
        </footer>
        <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- Yes Modal-->
<div class="modal fade" id="YesModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Apakah anda yakin dengan pilhan anda?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih "Oke" jika anda setujuh dengan pilihan anda.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <a class="btn btn-primary" href="#">Oke</a>
            </div>
        </div>
    </div>
</div>
<!-- No Modal-->
<div class="modal fade" id="NoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Berikan deskripsi penolakan</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">x</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input type="text" name="uk_req_motif" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                <button class="btn btn-primary" type="button" data-dismiss="modal">Simpan</button>
            </div>
        </div>
    </div>
</div>