<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Data Karyawan Baru</h1>
    <div class="padd">
        <a href="<?php echo base_url('Datakaryawan'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <!-- form add data karyawan-->
    <div class="container-fluid">
        <form action="<?php echo base_url('Datakaryawan/proses_tambah_data'); ?>" method="post">
            <div class="panel panel-default">
                <div class="panel-body ov">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-nik-en">NIK</label>
                                <input required type="text" class="form-control" name="nik" id="input-nik-en" value="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-title-en">Nama Karyawan</label>
                                <input required type="text" class="form-control" name="nama_karyawan" id="input-nama_karyawan-en" value="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <label for="input-title-en">Jenis Kelamin</label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9 col-sm-12">
                    <div class="form-check">
                        <input required class="form-check-input" type="radio" name="jk" id="laki" value="m">
                        <label class="form-check-label" for="laki">Laki-laki</label>
                    </div>
                </div>
                <div class="col-md-9 col-sm-12">
                    <div class="form-check">
                        <input required class="form-check-input" type="radio" name="jk" id="perempuan" value="f">
                        <label class="form-check-label" for="perempuan">Perempuan</label>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-6 col-sm-3">
                    <div class="form-group">
                        <label for="input-tgl_lahir-en">Tanggal Lahir</label>
                        <input required type="date" value="" class="form-control" name="tgl_lahir" id="input-tgl_lahir-en" data-datepicker>
                    </div>
                </div>
                <div class="col-md-6 col-sm-3">
                    <div class="form-group">
                        <label for="input-tgl_kerja-en">Tanggal Kerja</label>
                        <input required type="date" value="" class="form-control" name="tgl_kerja" id="input-tgl_kerja-en" data-datepicker>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-id_divisi-en"> Divisi</label>
                        <select required class="form-control" name="id_divisi" id="input-id_divisi-en">
                            <option value="" disabled selected>---PILIH DIVISI---</option>
                            <?php foreach ($list_divisi as $ls) { ?>
                                <option value="<?php echo $ls->divisi_id ?>"> <?php echo $ls->nama_divisi; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-id_jabatan-en"> Jabatan</label>
                        <select required class="form-control" name="id_jabatan" id="input-id_jabatan-en">
                            <option value="" disabled selected>---PILIH JABATAN---</option>
                            <?php foreach ($list_jabatan as $lj) { ?>
                                <option value="<?php echo $lj->jabatan_id; ?>"> <?php echo $lj->nama_jabatan; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-phone-en">No Telp</label>
                        <input required type="tel" class="form-control" name="no_telp" id="input-no_telp-en" data-role="tagsinput" onkeypress="return event.charCode >= 8 && event.charCode <= 57" value="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-email-en">Email</label>
                        <input required type="email" class="form-control" name="email" id="input-email-en" value="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-pass-en">Username</label>
                        <input required type="text" class="form-control" name="username" id="input-pass-en" data-role="tagsinput" value="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-pass-en">Password</label>
                        <input required type="password" class="form-control" name="password" id="input-pass-en" data-role="tagsinput" value="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="form-group">
                        <label for="input-akses-en">Akses</label>
                        <div class="form-group">
							<select required class="form-control" name="akses" id="akses">
								<option value="" disabled selected>---PILIH AKSES---</option>
								<option value="user"> User</option>
								<option value="administrator"> Administrator</option>
							</select>
							</div>
                    </div>
                </div>
            </div>
            <div class="padd">
                <button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
                    Simpan
                </button>

                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Pilih "Simpan" jika data anda sudah benar?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                <button class="btn btn-primary" name="save">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>
