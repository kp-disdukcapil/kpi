<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit KPI Grup</h1>
    <div class="padd">
        <a href="KpiGrup" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <form action="" method="post">
        <input type="hidden" name="_token" value="izY6nD00IoAw0Yn0auc6qgJPwPXDutjXU1ycmnUe">
        <div class="panel panel-default">
            <div class="panel-body ov">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-title-en">KPI Group Name</label>
                            <div class="input-language" data-lang="en"><input type="text" class="form-control" name="title[en]" id="input-title-en" value="teknisi"></div>
                            <div class="input-language" data-lang="id" style="display:none"><input type="text" class="form-control" name="title[id]" id="input-title-id" value=""></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="input-id_divisi-en">Untuk Divisi</label>
                            <select class="form-control" name="id_divisi[en]" id="input-id_divisi-en">
                                <option value=""></option>
                                <option value="6">IT</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="input-id_jabatan-en">Untuk Jabatan</label>
                            <select class="form-control" name="id_jabatan[en]" id="input-id_jabatan-en">
                                <option value=""></option>
                                <option value="4">System Analyst</option>
                                <option value="3">Staff IT</option>
                                <option value="2" selected>Supervisor</option>
                                <option value="1">General Manager</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-description-en">Description</label>
                            <div class="input-language" data-lang="en"><textarea class="form-control" name="description[en]" id="input-description-en" data-tinymce><p>das</p></textarea></div>
                            <div class="input-language" data-lang="id" style="display:none"><textarea class="form-control" name="description[id]" id="input-description-id" data-tinymce></textarea></div>
                        </div>
                    </div>
                </div>
                <div class="row"> </div>

                <div class="padd">
                    <button class="btn btn-primary">Save</button>
                </div>

            </div>
        </div>
    </form>
</div>
</div>