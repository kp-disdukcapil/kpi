<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Ubah Data Divisi</h1>
    <div class="padd">
        <a href="<?php echo base_url('DataDivisi'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>

<!-- form add data-->
	<div class="container-fluid">
		<form action="<?php echo base_url('Datadivisi/proses_ubah_data'); ?>" method="post">
			<input type="hidden" name="_token" value="xdFn5BPCdZ9ypR0Qz8wSltQJ0CMMyXFukF1m0Nh2">
			<div class="panel panel-default">
				<div class="panel-body ov">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label for="input-title-en">Nama Divisi</label>
								<input type="hidden" class="form-control" name="divisi_id" id="input-title-en" value="<?php echo $data_divisi->divisi_id; ?>">
								<input required type="text" class="form-control" name="nama_divisi" id="input-title-en" value="<?php echo $data_divisi->nama_divisi; ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label for="input-id_jabatan-en"> Jabatan</label>
							<select required class="form-control" name="jabatan_id" id="input-id_jabatan-en">
								<option value="" disabled selected>---PILIH JABATAN---</option>
								<?php foreach ($list_jabatan as $lj) { ?>
									<option value="<?php echo $lj->jabatan_id; ?>" <?= $lj->jabatan_id == $data_divisi->jabatan_id ? 'selected' : ''; ?>> <?php echo $lj->nama_jabatan; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="form-group">
						<label for="input-description-en">Deskripsi</label>
						<textarea required class="form-control" name="deskripsi" id="input-description-en" data-tinymce><?php echo $data_divisi->deskripsi; ?></textarea>
					</div>
				</div>
			</div>

			<div class="row"> </div>
			<button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
				Simpan
			</button>
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body">
							Pilih "Simpan" jika data anda sudah benar?
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
							<button class="btn btn-primary" name="save">Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</form>
