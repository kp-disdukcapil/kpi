    <br><!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Lengkap</th>
                            <th>Devisi</th>
                            <th>jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($list_penilai as $row) { ?>
                            <tr>
                                <td><?php echo ++$no; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->nama_devisi; ?></td>
                                <td><?php echo $row->nama_jabatan; ?></td>
                                <td>
                                    <div class="padd">
                                        <a href="<?php echo base_url('DataPenilai/form_ubah/') . $row->penilai_id ?>" class="btn btn-dark">
                                            <i class="fas fa-fw fa-solid fa-pen"></i>Ubah</a>
                                        <button type="button" class="btn btn-danger" name="Hapus" data-toggle="modal" data-target="#exampleModal-<?= $row->penilai_id ?>">
                                            <i class="fas fa-fw fa-solid fa-trash"></i>
                                            Hapus
                                        </button>
                                        <div class="modal fade" id="exampleModal-<?= $row->penilai_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Pilih "Hapus" jika anda yakin ingin menghapus data ini?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                        <a href="<?php echo base_url('DataPenilai/proses_hapus_data/') . $row->penilai_id ?>" class="btn btn-danger">Hapus</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<script type="text/javascript">
    windows.print();
    </script>