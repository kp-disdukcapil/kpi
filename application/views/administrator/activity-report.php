<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Rekap Data</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3"></div>
        <div class="card-body">
            <form action="<?php echo base_url('ActivityReport/export'); ?>" method="post" class="report-form">
<!--                <input type="hidden" name="_token" value="ih99ycWZthjUhPwUgVgGyqk2rg5ONwozFHUla6F6">-->
                <div class="row">
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label>Start Date</label>
                            <div class="input-daterange input-group" id="tgl_start">
                                <input type="date" class="input-sm form-control" name="periode_start" autocomplete="off" value="">
                            </div>
                        </div>
                    </div>
					<div class="col-sm-2">
						<div class="form-group">
							<label>End Date</label>
							<div class="input-daterange input-group" id="tgl_end">
								<input type="date" class="input-sm form-control" name="periode_end" autocomplete="off" value="">
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-3">
						<div class="form-group">
							<label for="input-id_divisi-en"> User</label>
							<select class="form-control" name="user_id" id="input-user_id-en">
								<option value="0" disabled selected>---Pilih User---</option>
								<?php foreach ($list_user as $lu) { ?>
									<option value="<?php echo $lu->user_id ?>"> <?php echo $lu->nama_user; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label for="input-id_divisi-en"> Divisi</label>
							<select class="form-control" name="id_divisi" id="input-id_divisi-en">
								<option value="0" disabled selected>---Pilih Divisi---</option>
								<?php foreach ($list_divisi as $ls) { ?>
									<option value="<?php echo $ls->divisi_id ?>"> <?php echo $ls->nama_divisi; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label for="input-id_grup-en"> Grup</label>
							<select class="form-control" name="id_grup" id="input-id_grup-en">
								<option value="0" disabled selected>---Pilih Grup---</option>
								<?php foreach ($list_grup as $lg) { ?>
									<option value="<?php echo $lg->grup_id ?>"> <?php echo $lg->nama_grup; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label for="input-id_item-en"> Item</label>
							<select class="form-control" name="id_item" id="input-id_item-en">
								<option value="0" disabled selected>---Pilih Item---</option>
								<?php foreach ($list_item as $li) { ?>
									<option value="<?php echo $li->item_id ?>"> <?php echo $li->parameter; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
                <div align="center">
                    <button class="btn btn-primary">Cetak Laporan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
	var collapseActivity = document.getElementById('collapseActivity');
	var activityreport = document.getElementById('activityreport');
	var activitymanager = document.getElementById('activitymanager');
	collapseActivity.classList.add("show");
	activityreport.classList.add("active");
	activitymanager.classList.add("active");
</script>
