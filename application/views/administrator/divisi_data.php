<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Data Divisi</h1>
    <div class="padd">
        <a href="<?php echo base_url('DataDivisi/form_tambah'); ?>" class="btn btn-primary">Tambah Divisi</a>
    </div>
    <br>
<!--    <div class="padd">-->
<!--        <a href="--><?php //echo base_url('DataDivisi/export'); ?><!--" class="btn btn-primary">export</a>-->
<!--    </div>-->
<!--    <br>-->
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
			<?php echo $this->session->flashdata('pesan'); ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Divisi</th>
                            <th>Nama Jabatan</th>
                            <th>Deskripsi</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($list_divisi as $row) { ?>
                            <tr>
                                <td><?php echo ++$no; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <td><?php echo $row->nama_jabatan; ?></td>
                                <td><?php echo $row->deskripsi; ?></td>
                                <td>
                                    <div class="padd">
                                        <a href="<?php echo base_url('Datadivisi/form_ubah/') . $row->divisi_id ?>" class="btn btn-dark">
                                            <i class="fas fa-fw fa-solid fa-pen"></i>Ubah</a>
                                        <button type="button" class="btn btn-danger" name="Hapus" data-toggle="modal" data-target="#exampleModal-<?= $row->divisi_id ?>">
                                            <i class="fas fa-fw fa-solid fa-trash"></i>
                                            Hapus
                                        </button>
                                        <div class="modal fade" id="exampleModal-<?= $row->divisi_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Pilih "Hapus" jika anda yakin ingin menghapus data ini?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                        <a href="<?php echo base_url('Datadivisi/proses_hapus_data/') . $row->divisi_id ?>" class="btn btn-danger">Hapus</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    var collapseTwo = document.getElementById('collapseTwo');
    var datadivisi = document.getElementById('datadivisi');
    var datamaster = document.getElementById('datamaster');
    collapseTwo.classList.add("show");
    datadivisi.classList.add("active");
    datamaster.classList.add("active");
</script>
