<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Edit KPI Item</h1>
    <div class="padd">
        <a href="KpiItem" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <form action="" method="post">
        <input type="hidden" name="_token" value="QcfHTGyxdJui3Aqy3JEerCQiiRvax5AzbXM127Hx">
        <div class="panel panel-default">
            <div class="panel-body ov">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-id_kpi-en">KPI Group Used</label>
                            <select class="form-control" name="id_kpi[en]" id="input-id_kpi-en">
                                <option value=""></option>
                                <option value="9" selected>Analisa Sistem</option>
                                <option value="8">IT</option>
                                <option value="7">Kemampuan Berkomunikasi</option>
                                <option value="6">teknisi</option>
                                <option value="5">IT</option>
                                <option value="4">Member card</option>
                                <option value="3">Computer Skill KPI</option>
                                <option value="2">Communication KPI</option>
                                <option value="1">General KPI</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-sort_no-en">Sort No</label>
                            <div class="input-language" data-lang="en"><input type="number" class="form-control" name="sort_no[en]" id="input-sort_no-en" value="1"></div>
                            <div class="input-language" data-lang="id" style="display:none"><input type="number" class="form-control" name="sort_no[id]" id="input-sort_no-id" value=""></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-parameter-en">KPI Parameter</label>
                            <div class="input-language" data-lang="en"><textarea class="form-control" name="parameter[en]" id="input-parameter-en">Melakukan pengembangan aplikasi</textarea></div>
                            <div class="input-language" data-lang="id" style="display:none"><textarea class="form-control" name="parameter[id]" id="input-parameter-id"></textarea></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="input-bobot-en">Bobot</label>
                            <div class="input-language" data-lang="en"><input type="number" class="form-control" name="bobot[en]" id="input-bobot-en" min="0" step="1" value="50"></div>
                            <div class="input-language" data-lang="id" style="display:none"><input type="number" class="form-control" name="bobot[id]" id="input-bobot-id" min="0" step="1" value=""></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label for="input-minimum_threshold-en">Minimum Threshold</label>
                            <div class="input-language" data-lang="en"><input type="number" class="form-control" name="minimum_threshold[en]" id="input-minimum_threshold-en" min="0" step="1" value="0"></div>
                            <div class="input-language" data-lang="id" style="display:none"><input type="number" class="form-control" name="minimum_threshold[id]" id="input-minimum_threshold-id" min="0" step="1" value=""></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
                            <label for="input-maximum_threshold-en">Maximum Threshold</label>
                            <div class="input-language" data-lang="en"><input type="number" class="form-control" name="maximum_threshold[en]" id="input-maximum_threshold-en" min="0" step="1" value="100"></div>
                            <div class="input-language" data-lang="id" style="display:none"><input type="number" class="form-control" name="maximum_threshold[id]" id="input-maximum_threshold-id" min="0" step="1" value=""></div>
                        </div>
                    </div>
                </div>
                <div class="padd">
                    <button class="btn btn-primary">Save</button>
                </div>

            </div>
        </div>
    </form>
</div>
</div>