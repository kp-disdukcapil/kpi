<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Beranda</h1>
    </div>
    <div class="row">
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Data Karyawan</div>
                        </div>
                    </div>
                    <br>
                    <a href="Datakaryawan" class="btn btn-facebook btn-block">Masuk Ke Data Karyawan</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Data Divisi</div>
                        </div>
                    </div>
                    <br>
                    <a href="Datadivisi" class="btn btn-facebook btn-block">Masuk Ke Data Divisi</a>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-md-6 mb-4">
            <div class="card border-left-danger shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="h5 mb-0 font-weight-bold text-gray-800">Data Jabatan</div>
                        </div>
                    </div>
                    <br>
                    <a href="DataJabatan" class="btn btn-facebook btn-block">Masuk Ke Data Jabatan</a>
                </div>
            </div>
        </div>
    </div>

	<div class="row">
		<div class="col-xl-4 col-md-6 mb-4">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gray-800">Data KPI Grup</div>
						</div>
					</div>
					<br>
					<a href="KpiGrup" class="btn btn-facebook btn-block">Masuk Ke Data KPI Grup</a>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-md-6 mb-4">
			<div class="card border-left-warning shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gray-800">Data KPI Item</div>
						</div>
					</div>
					<br>
					<a href="KpiItem" class="btn btn-facebook btn-block">Masuk Ke Data KPI Item</a>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xl-4 col-md-6 mb-4">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gray-800">Data Activity</div>
						</div>
					</div>
					<br>
					<a href="Activity" class="btn btn-facebook btn-block">Masuk Ke Data Activity</a>
				</div>
			</div>
		</div>
		<div class="col-xl-4 col-md-6 mb-4">
			<div class="card border-left-success shadow h-100 py-2">
				<div class="card-body">
					<div class="row no-gutters align-items-center">
						<div class="col mr-2">
							<div class="h5 mb-0 font-weight-bold text-gray-800">Activity Report</div>
						</div>
					</div>
					<br>
					<a href="ActivityReport" class="btn btn-facebook btn-block">Masuk Ke Activity Report</a>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<script>
    var dashboard = document.getElementById('dashboard');
    dashboard.classList.add("active");
</script>
