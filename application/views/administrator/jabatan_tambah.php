<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Data Jabatan Baru</h1>
    <div class="padd">
        <a href="<?php echo base_url('Datajabatan'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>

    <!-- form add data-->
    <div class="container-fluid">
        <form action="<?php echo base_url('Datajabatan/proses_tambah_data'); ?>" method="post">
            <input type="hidden" name="_token" value="poXVUPHEIOuKyidpz7RhFZDcsYFj2iaOpw3Zosfb">
            <div class="panel panel-default">
                <div class="panel-body ov">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-group">
                                <label for="input-title-en">Nama Jabatan</label>
                                <input required type="text" class="form-control" name="nama_jabatan" id="input-nama_jabatan-en" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label for="input-description-en">Deskripsi</label>
                            <textarea required class="form-control" name="deskripsi" id="input-deskripsi-en" data-tinymce></textarea>
                        </div>
                    </div>
                </div>
                <div class="padd">
                    <button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
                        Simpan
                    </button>
                    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Pilih "Simpan" jika data anda sudah benar?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                    <button class="btn btn-primary" name="save">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </form>
    </div>
</div>
</div>
