<!-- Begin Page Content -->
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Score fill In</h1>
    <div class="panel panel-body">
        <form class="form-inline">
            <div class="form-group">
                <label>Lihat Penilaian Periode </label>
                <div>
                    <select name="bulan" class="form-control">
                        <option value=""></option>
                        <option value="1">January</option>
                        <option value="2">February</option>
                        <option value="3">March</option>
                        <option value="4">April</option>
                        <option value="5" selected>May</option>
                        <option value="6">June</option>
                        <option value="7">July</option>
                        <option value="8">August</option>
                        <option value="9">September</option>
                        <option value="10">October</option>
                        <option value="11">November</option>
                        <option value="12">December</option>
                    </select>

                    <input type="number" class="form-control" name="tahun" value="2022">

                    <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                </div>
            </div>
        </form>
    </div>
    <div class="padd">
        <a href="<?php echo base_url('Score/form_tambah'); ?>" class="btn btn-primary">Tambah Data</a>
    </div>
    <br>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No </th>
                            <th>Tanggal Penilaian</th>
                            <th>Nama Karyawan</th>
                            <th>Penilai</th>
                            <th>Score</th>
                            <th>Fill Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0;
                        foreach ($list_score as $row) { ?>
                            <tr>
                                <td><?php echo ++$no; ?></td>
                                <td><?php echo $row->tgl_penilaian; ?></td>
                                <td><?php echo $row->nama_karyawan; ?></td>
                                <td><?php echo $row->nama; ?></td>
                                <td><?php echo $row->score; ?></td>
                                <td>
                                    <center>
                                        <div class="padd">
                                            <a href="<?php echo base_url('Score/fillscore/') . $row->score_id ?>" class="btn btn-info">Fill Score</a>
                                        </div>
                                    </center>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
<script>
    var collapsePenilaian = document.getElementById('collapsePenilaian');
    var penilaian = document.getElementById('penilaian');
    var scorefillin = document.getElementById('scorefillin');
    collapsePenilaian.classList.add("show");
    scorefillin.classList.add('active');
    penilaian.classList.add("active");
</script>