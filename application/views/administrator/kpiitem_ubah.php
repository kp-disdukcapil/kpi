<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah KPI Item Baru</h1>
    <div class="padd">
        <a href="<?php echo base_url('KpiItem'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>

	<div class="container-fluid">
		<form action="<?php echo base_url('KpiItem/proses_ubah_data'); ?>" method="post">
			<input type="hidden" name="_token" value="QcfHTGyxdJui3Aqy3JEerCQiiRvax5AzbXM127Hx">
			<div class="panel panel-default">
				<div class="panel-body ov">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="input-id_kpi-en">Nama Grup KPI</label>
								<select required class="form-control" name="grup_id" id="input-grup_id">
									<option value="" disabled selected>---PILIH GRUP YANG DIGUNAKAN---</option>
									<?php foreach ($list_grup as $lg) { ?>
										<option value="<?php echo $lg->grup_id ?>" <?= $lg->grup_id == $data_item_kpi->grup_id ? 'selected' : ''; ?>> <?php echo $lg->nama_grup; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="input-parameter-en">KPI Parameter</label>
								<input type="hidden" class="form-control" name="item_id" id="input-grup" value="<?php echo $data_item_kpi->item_id; ?>">
								<input required type="text" class="form-control" name="parameter" id="input-title" value="<?php echo $data_item_kpi->parameter; ?>">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="input-bobot-en">Bobot</label>
								<input required type="number" class="form-control" name="bobot" id="input-bobot" min="0" step="1" value="<?php echo $data_item_kpi->bobot; ?>">
							</div>
						</div>
						<div class="col-md-3 col-sm-12">
							<div class="form-group">
								<label for="input-minimum_threshold-en">Nilai Minimum</label>
								<input required type="number" class="form-control" name="nilai_min" id="input-minimum_threshold-en" min="0" step="1" value="<?php echo $data_item_kpi->nilai_min; ?>">
							</div>
						</div>
						<div class="col-md-3 col-sm-12">
							<div class="form-group">
								<label for="input-maximum_threshold-en">Nilai Maximum</label>
								<input required type="number" class="form-control" name="nilai_max" id="input-maximum_threshold-en" min="0" step="1" value="<?php echo $data_item_kpi->nilai_max; ?>">
							</div>
						</div>
					</div>
					<div class="padd">
						<button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
							Simpan
						</button>
						<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										Pilih "Simpan" jika data anda sudah benar?
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
										<button class="btn btn-primary" name="save">Simpan</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
