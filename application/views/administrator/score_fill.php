<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Creat Fill in Score Data</h1>
    <div class="padd">
        <a href="<?php echo base_url('Score'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <form action="" method="post">
        <input type="hidden" name="_token" value="QEiPNMeQxf8Y5k7SfjKQdTNoTOqMGmPoF3SGnZrC">
        <div class="panel">
            <div class="panel-heading">
                <div class="panel-title">Analisa Sistem KPI</div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th width=50%>Parameter</th>
                            <th>Bobot</th>
                            <th width=40%>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Melakukan pengembangan aplikasi</td>
                            <td>50</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input type="number" data-irs data-from="0" data-to="100" data-realvalue="0" value="0" data-hash="2aa656d583e1272458ae76cdefb7a18e">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Melakukan riset kebutuhan</td>
                            <td>20</td>
                            <td>
                                <div class="row">
                                    <div class="col-sm-9">
                                        <input type="number" data-irs data-from="0" data-to="100" data-realvalue="0" value="0" data-hash="077785e25b9e41184492333abef3a3c9">
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="padd">
            <button class="btn btn-primary">Save Score</button>
        </div>
    </form>
</div>
</div>