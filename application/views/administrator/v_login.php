<head>
    <link href="<?php echo base_url(); ?>assets/css/styles.css" rel="stylesheet" type="text/css">
</head>
<div class="container">

    <body onload="hide_loading();">

    </body>

    <!-- Outer Row -->
    <div class="row justify-content-center">

        <div class="col-lg-7">

            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                    <br>
                    <br>
                    <!-- Nested Row within Card Body -->
                    <div class="row">
                        <div class="col-lg">
                            <div class="p-5">
                                <div class="text-center">
                                    <h1 class="h4 text-gray-900 mb-4">KPI DISDUKCAPIL</h1>
                                    <div class="loading overlay">
                                        <div class="lds-default">
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                            <div></div>
                                        </div>
                                    </div>
                                </div>
                                <form action="<?= site_url('Login/auth_login'); ?> " method="POST">
                                    <?= $this->session->flashdata('msg'); ?>
                                    <div class="form-group">
                                        <input required type="text" class="form-control form-control-user" id="username" name="username" placeholder="Username">
                                    </div>
                                    <div class="form-group">
                                        <input required type="password" class="form-control form-control-user" id="password" name="password" placeholder="Password">
                                    </div>
                                    <div class="form-group">
										<select required class="form-control" name="akses" id="akses">
											<option value="" disabled selected>---PILIH AKSES---</option>
											<option value="user"> User</option>
											<option value="administrator"> Administrator</option>
										</select>
                                    </div>
                                    <button type="submit" class="btn btn-danger btn-user btn-block">
                                        Login
                                    </button>
                                    <div id="login"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="assets/js/demo/script.js"></script>
</body>
</div>
