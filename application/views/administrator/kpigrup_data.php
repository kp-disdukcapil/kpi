<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">KPI Grup Data</h1>
    <div class="padd">
        <a href="<?php echo base_url('kpiGrup/form_tambah'); ?>" class="btn btn-primary">Tambah Grup</a>
    </div>
    <br>


    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        </div>
        <div class="card-body">
			<?php echo $this->session->flashdata('pesan'); ?>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama KPI Grup</th>
                            <th>Divisi</th>
                            <th>Jabatan</th>
                            <th>Deskripsi</th>
                            <th>Nilai</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php $no = 0;
                        foreach ($list_grup as $row) { ?>
                            <tr>
                                <td><?php echo ++$no; ?></td>
                                <td><?php echo $row->nama_grup; ?></td>
                                <td><?php echo $row->nama_divisi; ?></td>
                                <td><?php echo $row->nama_jabatan; ?></td>
                                <td><?php echo $row->deskripsi; ?></td>
                                <td><?php echo $row->nilai; ?> %</td>
                                <td>
                                    <div class="padd">
                                        <a href="<?php echo base_url('KpiGrup/form_ubah/') . $row->grup_id ?>" class="btn btn-dark">
                                            <i class="fas fa-fw fa-solid fa-pen"></i>Ubah</a>
                                        <button type="button" class="btn btn-danger" name="Hapus" data-toggle="modal" data-target="#exampleModal-<?= $row->grup_id ?>">
                                            <i class="fas fa-fw fa-solid fa-trash"></i>
                                            Hapus
                                        </button>
                                        <div class="modal fade" id="exampleModal-<?= $row->grup_id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        Pilih "Hapus" jika anda yakin ingin menghapus data ini?
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                                        <a href="<?php echo base_url('KpiGrup/proses_hapus_data/') . $row->grup_id ?>" class="btn btn-danger">Hapus</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php
//servername
$servername = "localhost";
//username
$username = "root";
//empty password
$password = "";
//database is the database name
$dbname = "kpi";
 
// Create connection by passing these connection parameters
$conn = new mysqli($servername, $username, $password, $dbname);
 
//sql query to find minimum salary
$sql = "SELECT t1.*
FROM activity t1
INNER JOIN (
    SELECT activity_id, item_id, MAX(tgl_activity) maxdate
    FROM activity
    GROUP BY item_id
) t2
ON t1.item_id = t2.item_id AND t1.tgl_activity = t2.maxdate";
//display data on web page
$result = $conn->query($sql);
$progres = array();
while ($r = mysqli_fetch_array($result)){
    // echo "progres :". $r['progres'];
    $progres[$r['item_id']]=$r['progres'];
    // array_push($progres,$r['progres']);
    echo "<br />";
}
// echo var_dump($progres) ;


//close the connection
 
$conn->close();
?>
            </div>
        </div>
    </div>
</div>
</div>


<script>
    var collapseKPI = document.getElementById('collapseKPI');
    var kpigrup = document.getElementById('kpigrup');
    var kpimanager = document.getElementById('kpimanager');
    collapseKPI.classList.add("show");
    kpimanager.classList.add("active");
    kpigrup.classList.add("active");
</script>
