<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Data Pengguna Baru</h1>
    <div class="padd">
        <a href="<?php echo base_url('Datauser'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <div class="container-fluid">
        <form action="<?php echo base_url('Datauser/proses_tambah_data'); ?>" method="post">
            <input type="hidden" name="_token" value="m64BlumwEkcDKcoOmSGVwABb1tEcbXMPAH0UG7kn">
            <div class="panel panel-default">
                <div class="panel-body ov">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-nama_user-en">Nama</label>
                                <input type="text" class="form-control" name="nama_user" id="input-nama_user-en" value="">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-username-en">Username</label>
                                <input type="text" class="form-control" name="username" id="input-username-en" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="input-password-en">Password</label>
                            <input type="password" class="form-control" name="password" id="input-password-en" minlength="8" value="">
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label for="input-role_id-en">Akses</label>
                            <input type="text" class="form-control" name="akses" id="input-akses-en" value="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row"> </div>

            <div class="padd">
                <button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
                    Simpan
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Pilih "Simpan" jika data anda sudah benar?
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                <button class="btn btn-primary" name="save">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>