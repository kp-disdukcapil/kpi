<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Data Karyawan Baru</h1>
    <div class="padd">
        <a href="<?php echo base_url('Score'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>
    <!-- form add data karyawan-->
    <div class="container-fluid">
        <form action="<?php echo base_url('Score/proses_tambah_data'); ?>" method="post">
            <input type="hidden" name="_token" value="xdFn5BPCdZ9ypR0Qz8wSltQJ0CMMyXFukF1m0Nh2">
            <div class="panel panel-default">
                <div class="panel-body ov">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-karyawan_id-en"> Nama Karyawan</label>
                                <select class="form-control" name="karyawan_id" id="input-karyawan_id-en">
                                    <option value="0" disabled selected>---PILIH KARYAWAN YANG AKAN DINILAI---</option>
                                    <?php foreach ($list_karyawan as $lk) { ?>
                                        <option value="<?php echo $lk->karyawan_id ?>"> <?php echo $lk->nama_karyawan; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label for="input-penilai_id-en"> Penilai</label>
                                <select class="form-control" name="penilai_id" id="input-penilai_id-en">
                                    <option value="0" disabled selected>---PILIH PENILAI---</option>
                                    <?php foreach ($list_penilai as $lp) { ?>
                                        <option value="<?php echo $lp->penilai_id; ?>"> <?php echo $lp->nama; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <div class="form-group">
                                <label for="input-tgl_penilaian-en">Tanggal Penilaian</label>
                                <input type="date" value="" class="form-control" name="tgl_penilaian" id="input-tgl_penilaian-en" data-datepicker>
                            </div>
                        </div>
                    </div>
                    <div class="padd">
                        <button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
                            Simpan
                        </button>
                        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Pilih "Simpan" jika data anda sudah benar?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                                        <button class="btn btn-primary" name="save">Simpan</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
</div>