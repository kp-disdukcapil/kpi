<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Tambah Activity Data</h1>
    <div class="padd">
        <a href="<?php echo base_url('Dashboard'); ?>" class="btn btn-primary">Kembali</a>
    </div>
    <br>

	<div class="container-fluid">
		<form action="<?php echo base_url('Dashboard/proses_tambah_data'); ?>" method="post">
			<div class="panel panel-default">
				<div class="panel-body ov">
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
								<label for="input-parameter-en">Parameter</label>
								<select required class="form-control" name="parameter" id="input-parameter-en">
									<option value="" disabled selected>---PILIH PARAMETER---</option>
									<?php foreach ($list_item as $li) { ?>
										<option value="<?php echo $li->item_id ?>"> <?php echo $li->parameter; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-md-4 col-sm-3">
							<div class="form-group">
								<label for="input-tgl_aktifitas-en">Tgl Aktifitas</label>
								<input required type="date" value="" class="form-control" name="tgl_activity" id="input-tgl_aktifitas-en" data-datepicker>
							</div>
						</div>
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
								<label for="input-progres-en">Progres</label>
								<input required type="number" class="form-control" name="progres" id="input-progres-en" value=""></input>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="form-group">
								<label for="input-description-en">Description</label>
								<textarea required class="form-control" name="deskripsi" id="input-description-en" data-tinymce></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="padd">
					<button type="button" class="btn btn-primary" name="save" data-toggle="modal" data-target="#exampleModal">
						Simpan
					</button>
					<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">Perhatian!</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									Pilih "Simpan" jika data anda sudah benar?
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
									<button class="btn btn-primary" name="save">Simpan</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
