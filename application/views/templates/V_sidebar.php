<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">
        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('Dashboard'); ?>">
            <div class="sidebar-brand-icon rotate-n-15">
            </div>
            <div class="sidebar-brand-text mx-3">Disdukcapil<br>Pekanbaru</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item" id="dashboard">
            <a class="nav-link active" href="<?php echo base_url('Dashboard'); ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider">
        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item " id="datamaster">
            <a class="nav-link active" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-solid fa-database"></i>
                <span>Master</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" id="datakaryawan" href="<?php echo base_url('DataKaryawan'); ?>">Data Karyawan</a>
                    <a class="collapse-item" id="datadivisi" href="<?php echo base_url('Datadivisi'); ?>">Data Divisi</a>
                    <a class="collapse-item" id="datajabatan" href="<?php echo base_url('DataJabatan'); ?>">Data Jabatan</a>
                </div>
            </div>
        </li>
        <li class="nav-item" id="kpimanager">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseKPI" aria-expanded="true" aria-controls="collapseKPI">
                <i class="fas fa-fw fa-solid fa-dollar-sign"></i>
                <span>KPI Manager</span>
            </a>
            <div id="collapseKPI" class="collapse" aria-labelledby="headingKPI" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" id="kpigrup" href="<?php echo base_url('KpiGrup'); ?>">KPI Grup Data</a>
                    <a class="collapse-item" id="kpiitem" href="<?php echo base_url('KpiItem'); ?>">KPI Item Data</a>
                </div>
            </div>
        </li>
        <li class="nav-item" id="activitymanager">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseActivity" aria-expanded="true" aria-controls="collapseActivity">
                <i class="fas fa-fw fa fa-paperclip"></i>
                <span>Activity</span>
            </a>
            <div id="collapseActivity" class="collapse" aria-labelledby="headingActivity" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" id="activity" href="<?php echo base_url('Activity'); ?>">Data</a>
					<a class="collapse-item" id="activityreport" href="<?php echo base_url('ActivityReport'); ?>">Report</a>
                </div>
            </div>
        </li>
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        <br>
        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>
                <form class="form-inline">
                </form>

                <!-- Topbar Search -->
                <form class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
                    <div class="input-group">

                        <div class="input-group-append">

                        </div>
                    </div>
                </form>

                <!-- <div class="topbar-divider d-none d-sm-block"></div> -->
                <!-- Nav Item - User Information -->
                <ul style="list-style-type: none;">
                    <li class="nav-item dropdown no-arrow">
                        <button style="background:none;border:none;" class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?= ucwords($username['username']); ?></span>
                            <img class="img-profile rounded-circle" src="<?php echo base_url() . "/assets/img/undraw_profile.svg"; ?>">
                        </button>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <div class="dropdown-divider"></div>
                            <button onclick="xxx()" class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModals">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </button>
                        </div>
                    </li>

                </ul>

            </nav>
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Anda Yakin Ingin Keluar?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Pilih "Keluar" di bawah jika Anda siap untuk mengakhiri sesi Anda saat ini.</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="<?= base_url('Login/logout'); ?>">Logout</a>
                        </div>
                    </div>
                </div>
            </div>

            <style>
                #loading {
                    width: 50px;
                    height: 50px;
                    border: solid 5px #CCC;
                    border-top-color: #ff6a00;
                    border-radius: 100%;
                }
            </style>
